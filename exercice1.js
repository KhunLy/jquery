//$(document).ready(function() {
// =
$(function() {
    //code ici
    $('#select_brand').on('change', function(e) {
        // value => valeur courante du select
        let value = $(e.target).val();
        // cacher tous les "li"
        $('li').fadeOut(0);
        // selectionner les "li" que l'on veut afficher 
        $('.'+value).fadeIn(500);
        // et les faire réapparaitre
    });
});


$(function() {
    $('li').hide(0);

    $('input[type=checkbox]').on('change', function(e) {
        //let value = $(e.target).val();
        let value = $(e.target).attr('name');
        if($(e.target).is(':checked')) {
            //$('li[category=' + value + ']').show(500);
            $(`li[category=${value}]`).show(500);
        }
        else {
            $('li[category=' + value + ']').hide(500);
        }
    });

});
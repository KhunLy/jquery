$(document).ready(function() {
    //selection les petites images + ajout click
    $('.smallImg > img').on('mouseover', function(e){
        // selectionner l'image sur laquelle on a cliqué
        // + recupérer son attribut src
        let source = $(e.target).attr('src');
        // redéfinir l'attribut source de la grande image
        $('.largeImg > img')
            .fadeOut(0)
            .attr('src', source)
            .fadeIn(300);
    });
});